Get contents of static/ folder in one of two ways:
1.) from https://drive.google.com/drive/folders/1rMzZEiADUuBu3hAKj0TgSjSeNsv9mEaF?usp=sharing
2.) run the movie pickler https://bitbucket.org/vyabor/movie-pickler

Files are:
	KNN.sav
	movie_ratings.sav
	tfidf_vec.sav
	vectorizer2.sav
	classifier.sav
	multilabel_binarizer.sav
	multilabel_binarizer_similar.sav
	indices.sav
	cos_sim.sav